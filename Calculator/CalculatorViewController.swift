//
//  CalculatorViewController.swift
//  Calculator
//
//  Created by Витя on 3/31/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController, UIPageViewControllerDelegate,UIPageViewControllerDataSource, UIScrollViewDelegate {
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    lazy var palindrome: PalindromeViewController = {
        
        var viewController = storyboard?.instantiateViewController(withIdentifier: "Palindrome") as! PalindromeViewController
        return viewController
    }()
    
    lazy var factorial: FactorialViewController = {
        
        var viewController = storyboard?.instantiateViewController(withIdentifier: "Factorial") as! FactorialViewController
        return viewController
    }()
    
    lazy var pairs: PairsViewController = {
        
        var viewController = storyboard?.instantiateViewController(withIdentifier: "Pairs") as! PairsViewController
        return viewController
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        ssss()
        currentPage = 0
        createPageViewController()
        arrVC.append(palindrome)
        arrVC.append(factorial)
        arrVC.append(pairs)
    }
    var segmentedControl: CustomSegmentedControl!
    func ssss() {
        segmentedControl = CustomSegmentedControl.init(frame: CGRect.init(x: 0, y: 25, width: self.view.frame.width, height: 45))
        segmentedControl.backgroundColor = UIColor(red: 63/255.0, green: 81/255.0, blue: 181/255.0, alpha: 1.0)
        segmentedControl.commaSeperatedButtonTitles = "PALINDROME, FACTORIAL, PAIRS"
        segmentedControl.addTarget(self, action: #selector(onChangeOfSegment(_:)), for: .valueChanged)
        
        self.view.addSubview(segmentedControl)
    }
    private func createPageViewController() {
        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.pageController.view.frame = CGRect(x: 0, y: self.segmentedControl.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
        }
        pageController.setViewControllers([palindrome], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        self.addChild(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParent: self)
    }
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.firstIndex(of: viewCOntroller)!
        }
        
        return -1
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index - 1
        }
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index + 1
        }
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed) {
            currentPage = arrVC.firstIndex(of: (pageViewController1.viewControllers?.last)!)
            self.segmentedControl.updateSegmentedControlSegs(index: currentPage)
            
        }
        
    }
    @objc func onChangeOfSegment(_ sender: CustomSegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            pageController.setViewControllers([arrVC[0]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
            currentPage = 0
            
        case 1:
            if currentPage > 1{
                pageController.setViewControllers([arrVC[1]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
                currentPage = 1
            }
            else{
                pageController.setViewControllers([arrVC[1]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                currentPage = 1
            }
        case 2:
            if currentPage < 2 {
                pageController.setViewControllers([arrVC[2]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                currentPage = 2
            }
            else{
                pageController.setViewControllers([arrVC[2]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
                currentPage = 2
            }
        default:
            break
        }
    }
}
