//
//  FactorialViewController.swift
//  Calculator
//
//  Created by Витя on 4/2/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class FactorialViewController: UIViewController {
    typealias BigInt = _BigInt<UInt>
    @IBOutlet weak var numTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBAction func Calculate(_ sender: UIButton) {
        let number = BigInt(numTextField.text!)!
        var resultFactorial = factorial(n: number)
        var sum:FactorialViewController.BigInt = 0
        while (resultFactorial != 0) {
            let remainder = resultFactorial % 10
            sum = sum + remainder
            resultFactorial = resultFactorial / 10
        }
        resultLabel.text = ("\(sum)")
}
    override func viewDidLoad() {
        super.viewDidLoad()
        numTextField.underlined()
    }
    @discardableResult
    func factorial(n: BigInt) -> BigInt {
        return n < 2 ? 1 : n * factorial(n: n-1)
    }
}
    

