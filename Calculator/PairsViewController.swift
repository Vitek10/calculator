//
//  PairsViewController.swift
//  Calculator
//
//  Created by Витя on 4/2/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class PairsViewController: UIViewController {
    @IBOutlet weak var numTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBAction func Calculate(_ sender: UIButton) {
        var arr =   [(1,4), (2,5), (7,3), (4,6), (7,7)]
        numTextField.text = "\(arr)"
        var curr = [(Int, Int)]()
        var max = [(Int, Int)]()
        curr.append(arr[0])
        for element in arr.dropFirst(){
            if curr.last!.0 < element.0 && curr.last!.1 > element.1{
                curr.append(element)
            }else{
                max = curr.count > max.count ? curr : max
                curr = [element]
            }
        }
        resultLabel.text = "\(max)"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        numTextField.underlined()
    }
}
