//
//  PalindromeViewController.swift
//  Calculator
//
//  Created by Витя on 4/2/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class PalindromeViewController: UIViewController {
    @IBOutlet weak var numTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBAction func Calculate(_ sender: UIButton) {
        var number = Int(numTextField.text!)!
        var reversed = 0; var rem = 0; let n = number
        while number != 0 {
        rem = number%10
        reversed = reversed * 10 + rem
        number /= 10
         }
        n == reversed ? (resultLabel.text = "Palindrome") :
            (resultLabel.text = "Is not palindrome")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        numTextField.underlined()
        // Do any additional setup after loading the view.
    }

}
