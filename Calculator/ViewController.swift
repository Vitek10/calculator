//
//  ViewController.swift
//  Calculator
//
//  Created by Витя on 3/29/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var userName: UITextField!{
        didSet {
            userName.tintColor = UIColor.white
            userName.setIcon(#imageLiteral(resourceName: "icon_user"))
        }
    }
    @IBOutlet weak var passWord: UITextField!{
        didSet {
            passWord.tintColor = UIColor.white
            passWord.setIcon(#imageLiteral(resourceName: "icon_pass"))
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.underlined()
        passWord.underlined()
    }
}

